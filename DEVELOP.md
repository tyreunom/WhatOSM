# Develop

This application is based on web technologies, and mainly written in ECMAScript 6 (modern JavaScript). This documentation is for people who want to build by themselves or develop the project. If you just want to install it somewhere, you can download one of the [recent builds](http://projets.pavie.info/whatosm/builds/), and put it on your FTP server.


## Dependencies

You need to install the given tools before starting:
* Node Package Manager (NPM) - [Documentation](https://docs.npmjs.com/getting-started/what-is-npm)
* Transifex CLI - [Documentation](https://docs.transifex.com/client/installing-the-client)


## Build your own

To build and deploy your own instance, run the following commands:
```sh
npm install # Retrieve dependencies
npm run build # Test and compile
```

Then, if everything went well, you have now a `build/` folder and `WhatOSM_latest.zip` which are ready to deploy on any web server.


## Develop locally

You can also run a develop web server for testing purposes. To do so, run these commands:
```sh
npm run start
```

If no errors appears, a web server at `http://localhost:3000` is ready to browse.


## Pull requests

As this project is open source, pull requests are welcome. If you want to improve project, or add a new tool, please create a new branch based on __develop__ (not master).


## Additional information

### Locales

Various text labels are present on view components. To make the application understandable in several languages, we use locales files, which allow to translate those labels. Locales are managed using Transifex. You can see [the project here](https://www.transifex.com/openlevelup/whatosm/).

To manage locales locally, you can use these commands:
```sh
npm run i18n:download       # Retrieve locales from Transifex, having > 75% of translated labels
npm run i18n:build          # Regenerate en locale file when labels have been added in code
npm run i18n:upload         # Upload en locale file to Transifex (to do when file has been regenerated)
npm run i18n:status         # Show current status of locales files
```

To add a new locale (which has never been integrated before), please do the following:
* Retrieve locale file with `npm run i18n:download`
* Edit `src/app/app.js`, add the locale code (like _fr_ or _fr-FR_) in the `LOCALES` array (near the top of the file)
* Check that the locale is working as expected by running `npm run start` and changing your browser locale

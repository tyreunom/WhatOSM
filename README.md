# Which tool for OSM ?

__Which tool for OSM ?__ is a website helping you choose your next thematic contribution to OpenStreetMap, according to simple parameters: where you are, how much time you want to spend, and your level of expertise.

You can try it out at [projets.pavie.info/whatosm](http://projets.pavie.info/whatosm/) (or [develop instance](http://projets.pavie.info/whatosm/dev/)).

[![Help making this possible](https://liberapay.com/assets/widgets/donate.svg)](https://liberapay.com/PanierAvide/donate)

## Purpose

OpenStreetMap offers a wide ecosystem of tools. As it is very open, most of the tools are hosted on different places. Most of them have a bit of documentation on OpenStreetMap wiki But you can easily miss some cool tools because you don't have all your lifetime to watch the OSM wiki. "Which tool for OSM ?" is here to help you find cool contribution tools, corresponding to your needs, in a few seconds. It is aimed to work on desktop and smartphone, offering you the ability to contribute everywhere.


## Contributing

This project is an open source tool, meaning it is made by volunteers. If you want to help us, there are several ways (technical or non-technical ones). You can find how to participate in the [contributing documentation](CONTRIBUTING.md).


## Develop/run your own instance

"Which tool for OSM ?" is already available online for use (see link on top of this document). If you want to run your own, you can grab one of the [recent builds](http://projets.pavie.info/whatosm/builds/). To work on project development, you can read [develop documentation](DEVELOP.md).


## License

Copyright 2017 Adrien PAVIE

See [LICENSE](LICENSE.txt) for complete AGPL3 license.

"Which tool for OSM ?" is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

"Which tool for OSM ?" is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with "Which tool for OSM ?". If not, see http://www.gnu.org/licenses/.


### Use of OSM in the name

Even if this project allows to find easily contribution tools for OpenStreetMap, __it is not an official OSM Foundation product, and it is neither endorsed nor sponsored by OSM Foundation__. It only uses _OSM_ in its name to make it obvious that this is a community project related to OpenStreetMap. To make this clearer, it was renamed _Which tool for OSM ?_ (original name being _WhatOSM_).

To know more about this disclaimer, please read the [trademark policy of OpenStreetMap](https://wiki.openstreetmap.org/wiki/Trademark_Policy).


### Libraries

* [Material UI Webpack](https://github.com/callemall/material-ui-webpack-example) - MIT License

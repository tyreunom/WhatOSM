/**
 * Reads all JSON tools description in src/config/tools and create a bundled version.
 */

const fs = require('fs');

const TOOLS_DIR = "./src/config/tools";
const OUTPUT_FILE = "./src/www/tools.js";
const OUTPUT_FILE_I18N = "./src/config/locales/tools/en.json";
const JSON_RGX = /^[A-Za-z0-9_\-]+\.json$/;
const URL_RGX = /^https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)$/;
const IMG_FORMAT_RGX = /(png|PNG|jpg|JPG|jpeg|JPEG)$/;
const DURATIONS = [ "seconds", "minutes", "hours", "days" ];
const LOCATIONS = [ "inside", "outside", "everywhere" ];
const DIFFICULTIES = [ "easy", "intermediate", "difficult", "complex" ];

const bundle = { };
const i18ntools = {};
let count = 0;

//Read tool files
fs.readdirSync(TOOLS_DIR).forEach((file) => {
	if(JSON_RGX.test(file)) {
		const toolId = file.substring(0, file.length - 5);
		try {
			//Add to bundle
			bundle[toolId] = JSON.parse(fs.readFileSync(TOOLS_DIR+"/"+file, 'utf8'));
			count++;
			
			/*
			 * Run some checks
			 */
			if(bundle[toolId].name === undefined || bundle[toolId].name.length < 3) {
				throw new Error("Invalid tool file: "+file+" (name missing or too short)");
			}
			
			if(bundle[toolId].catchphrase === undefined || bundle[toolId].catchphrase.length < 3 || bundle[toolId].catchphrase.length > 40) {
				throw new Error("Invalid tool file: "+file+" (catchphrase should be between 3 and 40 characters)");
			}
			
			if(bundle[toolId].logo !== undefined && bundle[toolId].logo !== "" && (!URL_RGX.test(bundle[toolId].logo) || !IMG_FORMAT_RGX.test(bundle[toolId].logo))) {
				throw new Error("Invalid tool file: "+file+" (logo should be a valid URL)");
			}
			
			if(!URL_RGX.test(bundle[toolId].picture) || !IMG_FORMAT_RGX.test(bundle[toolId].picture)) {
				throw new Error("Invalid tool file: "+file+" (picture should be a valid URL)");
			}
			
			if(bundle[toolId].description === undefined || bundle[toolId].description.length < 50) {
				throw new Error("Invalid tool file: "+file+" (description should be at least 50 characters long");
			}
			
			if(!URL_RGX.test(bundle[toolId].start_url)) {
				throw new Error("Invalid tool file: "+file+" (start_url should be a valid URL)");
			}
			
			if(!URL_RGX.test(bundle[toolId].doc_url)) {
				throw new Error("Invalid tool file: "+file+" (doc_url should be a valid URL)");
			}
			
			if(bundle[toolId].settings === undefined) {
				throw new Error("Invalid tool file: "+file+" (settings is mandatory)");
			}
			
			if(DURATIONS.indexOf(bundle[toolId].settings.duration) < 0) {
				throw new Error("Invalid tool file: "+file+" (settings.duration is invalid)");
			}
			
			if(LOCATIONS.indexOf(bundle[toolId].settings.where) < 0) {
				throw new Error("Invalid tool file: "+file+" (settings.where is invalid)");
			}
			
			if(DIFFICULTIES.indexOf(bundle[toolId].settings.difficulty) < 0) {
				throw new Error("Invalid tool file: "+file+" (settings.difficulty is invalid)");
			}
			
			//Add to translations
			i18ntools[toolId+"_c"] = bundle[toolId].catchphrase;
			i18ntools[toolId+"_d"] = bundle[toolId].description;
		}
		catch(e) {
			if(e instanceof SyntaxError) {
				throw new Error("Invalid tool file: "+file+" ("+e.message+")");
			}
			else {
				throw e;
			}
		}
	}
	else {
		throw new Error("Invalid file in "+TOOLS_DIR+": "+file);
	}
});

//Write into file the bundle
const outText = "var TOOLS = "+JSON.stringify(bundle)+"; if(module) { module.exports = TOOLS; }";

fs.writeFile(OUTPUT_FILE, outText, function(err) {
	if(err) {
		throw new Error(err);
	}
	else {
		console.log("Writed "+count+" tools files into "+OUTPUT_FILE);
	}
});

//Write translations into file
fs.writeFile(OUTPUT_FILE_I18N, JSON.stringify(i18ntools), function(err) {
	if(err) {
		throw new Error(err);
	}
	else {
		console.log("Created en locale file "+OUTPUT_FILE_I18N);
	}
});

import React, {Component} from 'react';
import AppBar from 'material-ui/AppBar';
import BuildIcon from 'material-ui/svg-icons/action/build';
import MailIcon from 'material-ui/svg-icons/communication/email';
import Drawer from 'material-ui/Drawer';
import MenuItem from 'material-ui/MenuItem';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import HelpIcon from 'material-ui/svg-icons/action/help';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import Settings from './Settings';
import Snackbar from 'material-ui/Snackbar';
import Subheader from 'material-ui/Subheader';
import ToolsGrid from './ToolsGrid';
import { green800, green900, deepOrange500 } from 'material-ui/styles/colors';

const styles = {
	container: {
	}
};

const muiTheme = getMuiTheme({
	palette: {
		primary1Color: green800,
		primary2Color: green900,
		accent1Color: deepOrange500
	}
});

class Main extends Component {
	constructor(props, context) {
		super(props, context);

		this.state = {
			sideOpen: false,
			stepIndex: 0,
			firstRendering: true,
			settings: {},
			snackOpen: false,
			snackMessage: ""
		};
	}
	
	/**
	 * Open/close the side menu
	 */
	sideToggle() {
		this.setState({
			sideOpen: !this.state.open
		});
	}
	
	/**
	 * Close the side menu
	 */
	sideClose() {
		this.setState({
			sideOpen: false
		});
	}
	
	snackClose() {
		this.setState({
			snackOpen: false
		});
	}
	
	/**
	 * Handler when a setting has changed in the Settings component
	 * @param {string} setting The setting name
	 * @param {string} value The setting value
	 */
	settingChange(setting, value) {
		let newSettings = {};
		newSettings[setting] = value;
		
		this.setState({
			settings: Object.assign({}, this.state.settings, newSettings)
		});
		
		if(!this.state.firstRendering) {
			this.settingsDone();
		}
	}
	
	/**
	 * Handler when all settings have been once defined by user.
	 */
	settingsDone() {
		if(!TOOLS) {
			this.showMessage(I18n.t("Can't load the list of OSM tools !"));
		}
		
		this.setState({
			firstRendering: false
		});
	}
	
	/**
	 * Shows the given message in a Snackbar
	 * @param {string} message The message to display
	 */
	showMessage(message) {
		this.setState({
			snackOpen: true,
			snackMessage: message
		});
	}

	render() {
		const appbar = <AppBar title={I18n.t("Which tool for OSM ?")} onLeftIconButtonTouchTap={this.sideToggle.bind(this)} />;
		let toolsgrid = null;
		
		if(!this.state.firstRendering) {
			toolsgrid = <ToolsGrid tools={TOOLS} settings={this.state.settings} />;
		}
		
		return (
			<MuiThemeProvider muiTheme={muiTheme}>
				<div style={styles.container}>
					{appbar}
					
					<Drawer
						open={this.state.sideOpen}
						docked={false}
						onRequestChange={(sideOpen) => this.setState({sideOpen})}
					>
						{appbar}
						<MenuItem
							href="https://framagit.org/PanierAvide/WhatOSM/blob/master/README.md"
							target="_blank"
							rightIcon={<HelpIcon />}
						>
							{I18n.t("About")}
						</MenuItem>
						<MenuItem
							href="https://framagit.org/PanierAvide/WhatOSM"
							target="_blank"
							rightIcon={<BuildIcon />}
						>
							{I18n.t("Source code")}
						</MenuItem>
						<MenuItem
							href="http://pavie.info/contact.html"
							target="_blank"
							rightIcon={<MailIcon />}
						>
							{I18n.t("Contact")}
						</MenuItem>
					</Drawer>
					
					<Subheader>{I18n.t("How do you want to contribute to OSM ?")}</Subheader>
					<Settings onChange={this.settingChange.bind(this)} onDone={this.settingsDone.bind(this)} />
					
					{toolsgrid}
					
					<Snackbar
						open={this.state.snackOpen}
						message={this.state.snackMessage}
						autoHideDuration={4000}
						onRequestClose={this.snackClose.bind(this)}
					/>
				</div>
			</MuiThemeProvider>
		);
	}
}

export default Main;

import React, {Component} from 'react';
import {GridList, GridTile} from 'material-ui/GridList';
import IconButton from 'material-ui/IconButton';
import MediaQuery from 'react-responsive';
import Subheader from 'material-ui/Subheader';
import Snackbar from 'material-ui/Snackbar';
import StarBorder from 'material-ui/svg-icons/toggle/star-border';
import ToolDetails from './ToolDetails';

const styles = {
	root: {
		display: 'flex',
		flexWrap: 'wrap',
		justifyContent: 'space-around'
	},
	gridList: {
		overflowY: 'auto',
		width: '100%'
	},
	gridTile: {
		cursor: 'pointer'
	}
};

const DIFFICULTIES = [ "easy", "intermediate", "difficult", "complex" ];
const DURATIONS = [ "seconds", "minutes", "hours", "days" ];

/**
 * ToolsGrid is the component for listing available tools.
 */
class ToolsGrid extends Component {
	constructor(props, context) {
		super(props, context);
		
		this.state = {
			toolDialog: null,
			toolDialogShouldOpen: false
		};
	}
	
	/**
	 * Filter tools according to settings, and creates the tile data list for grid display.
	 * @return {Object[]} The list of tiles to show in grid
	 */
	getTiles() {
		const tiles = [];
		
		//Check each tool
		for(const tId in this.props.tools) {
			const t = this.props.tools[tId];
			t.id = tId;
			
			let keep = true;
			
			if(
				(this.props.settings.where == t.settings.where || t.settings.where == "everywhere") //Same location
				&& (this.props.settings.duration == t.settings.duration || DURATIONS.indexOf(this.props.settings.duration) == DURATIONS.indexOf(t.settings.duration) + 1) //Same duration (or previous one)
				&& DIFFICULTIES.indexOf(this.props.settings.difficulty) >= DIFFICULTIES.indexOf(t.settings.difficulty) //Acceptable difficulty
			) {
				tiles.push({
					key: tId,
					img: t.picture,
					title: t.name,
					subtitle: t.catchphrase
				});
			}
		}
		
		return tiles;
	}
	
	showTool(id) {
		this.setState({
			toolDialog: this.props.tools[id],
			toolDialogShouldOpen: true
		});
	}
	
	onGridTileClick(id) {
		const idt = id;
		return () => {
			this.showTool(idt);
		};
	}
	
	render() {
		const tiles = this.getTiles();
		let snack = null;
		
		if(tiles.length == 0) {
			snack = <Snackbar open={true} message={I18n.t("No tools found for you !")} autoHideDuration={4000} />
		}
		
		return (
			<div style={styles.root}>
				<Subheader>{I18n.t("Corresponding tools")}</Subheader>
				
				<MediaQuery maxWidth={768}>
					<GridList
						cellHeight={180}
						cols={2}
						style={styles.gridList}
					>
						{tiles.map((tile) => (
							<GridTile
								key={tile.key}
								title={tile.title}
								subtitle={I18n.tt(tile.key+"_c")}
								style={styles.gridTile}
								onTouchTap={this.onGridTileClick(tile.key)}
							>
								<img src={tile.img} />
							</GridTile>
						))}
					</GridList>
				</MediaQuery>
				<MediaQuery minWidth={768}>
					<GridList
						cellHeight={180}
						cols={4}
						style={styles.gridList}
					>
						{tiles.map((tile) => (
							<GridTile
								key={tile.key}
								title={tile.title}
								subtitle={I18n.tt(tile.key+"_c")}
								style={styles.gridTile}
								onTouchTap={this.onGridTileClick(tile.key)}
							>
								<img src={tile.img} />
							</GridTile>
						))}
					</GridList>
				</MediaQuery>
				
				<ToolDetails ref="tooldetail" tool={this.state.toolDialog} />
				{snack}
			</div>
		);
	}
	
	componentDidUpdate() {
		setTimeout(() => {
			if(this.state.toolDialogShouldOpen) {
				this.refs.tooldetail.open();
				this.setState({
					toolDialogShouldOpen: false
				});
			}
		}, 100);
	}
}

export default ToolsGrid;

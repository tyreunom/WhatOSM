import React from 'react';
import {render} from 'react-dom';
import injectTapEventPlugin from 'react-tap-event-plugin';
import Main from './Main';
import I18n from 'i18nline/lib/i18n';

const LOCALES = [ "en", "fr", "de", "pt", "nl", "es", "hu" ];

/**
 * Application main launcher
 */

/*
 * Internationalization
 */

if(window.navigator.languages) {
     for(const l of window.navigator.languages) {
         if(LOCALES.includes(l)) {
             locale = l;
             break;
         }
     }
}

I18n.locale = locale || window.navigator.userLanguage || window.navigator.language;
I18n.fallbacks = true;

//Load translation files
for(const l of LOCALES) {
	Object.assign(I18n.translations, require("../config/locales/ui/"+l+".json"));
	Object.assign(I18n.translations[l], require("../config/locales/tools/"+l+".json"));
}

//Special function for tools
I18n.tt = (key) => {
	if(I18n.translations[I18n.locale] && I18n.translations[I18n.locale][key]) {
		return I18n.translations[I18n.locale][key];
	}
	else {
		const locale = I18n.locale.substring(0, 2);
		if(I18n.translations[locale] && I18n.translations[locale][key]) {
			return I18n.translations[locale][key];
		}
		else {
			return I18n.translations[I18n.defaultLocale] && I18n.translations[I18n.defaultLocale][key] || "[ Missing translation ]";
		}
	}
};

window.I18n = I18n;


/*
 * Component rendering
 */

injectTapEventPlugin();
render(<Main />, document.getElementById('app'));

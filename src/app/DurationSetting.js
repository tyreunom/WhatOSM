import React, {Component} from 'react';
import { RadioButton, RadioButtonGroup } from 'material-ui/RadioButton';
import { Step, Stepper, StepButton } from 'material-ui/Stepper';

const styles = {
	radioButton: {
		marginBottom: 15
	}
};

/**
 * DurationSetting is the component for selecting the amount of time the task should last.
 */
class DurationSetting extends Component {
	constructor(props, context) {
		super(props, context);
		
		this.state = {};
	}
	
	handleChange(event, value) {
		if(this.props.onChange) {
			this.props.onChange(value);
		}
	}
	
	render() {
		return (
			<RadioButtonGroup style={this.props.style} name="duration" defaultSelected={this.props.defaultValue} onChange={this.handleChange.bind(this)}>
				<RadioButton
					value="seconds"
					label={I18n.t("A few seconds")}
					style={styles.radioButton}
				/>
				<RadioButton
					value="minutes"
					label={I18n.t("A few minutes")}
					style={styles.radioButton}
				/>
				<RadioButton
					value="hours"
					label={I18n.t("A few hours")}
					style={styles.radioButton}
				/>
				<RadioButton
					value="days"
					label={I18n.t("A few days")}
					style={styles.radioButton}
				/>
			</RadioButtonGroup>
		);
	}
}

export default DurationSetting;
